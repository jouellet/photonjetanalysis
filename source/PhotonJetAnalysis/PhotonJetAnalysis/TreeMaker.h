#ifndef PhotonJetAnalysis_TreeMaker_H
#define PhotonJetAnalysis_TreeMaker_H

// Eventloop includes
#include <EventLoop/Algorithm.h>
#include "AsgTools/AnaToolHandle.h"

// Root includes
#include <TTree.h>
#include <TH1.h>

// GRL
#include "GoodRunsLists/GoodRunsListSelectionTool.h"

// Trigger
#include <TrigConfInterfaces/ITrigConfigTool.h>
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TriggerMatchingTool/MatchingTool.h"

// HI event includes
#include "xAODHIEvent/HIEventShapeContainer.h"

// MC truth classification
#include "MCTruthClassifier/MCTruthClassifier.h"

// ZDC
#include "ZdcAnalysis/ZdcAnalysisTool.h"

// E/gamma tools
#include "ElectronPhotonSelectorTools/AsgPhotonIsEMSelector.h"
#include "ElectronPhotonFourMomentumCorrection/EgammaCalibrationAndSmearingTool.h"

// Track selection tool
#include "InDetTrackSelectionTool/InDetTrackSelectionTool.h"

// Jet cleaning tool
#include "JetSelectorTools/JetCleaningTool.h"

// Jet calibration tool
#include "JetCalibTools/JetCalibrationTool.h"

// std includes
#include <string>
#include <vector>
#include <map>


enum CollisionSystem { pp15 = 0, PbPb15 = 1, pPb16 = 2, Pbp16 = 3, pp17 = 4, PbPb18 = 5 }; // pPb = period A, Pbp = period B
enum DataType { Collisions = 0, MCSignal = 1, MCDataOverlay = 2, MCHijing = 3, MCHijingOverlay = 4 };

class TreeMaker : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:

  const float m_trkPtCut = 1.;
  const float m_truthTrkPtCut = 4.5;
  const float m_jet4PtCut = 20.;
  const float m_truthJet4PtCut = 15.;
  const float m_photonPtCut = 5.;

  std::string m_outputName;
  CollisionSystem m_collisionSystem;
  DataType m_dataType;

  const std::string m_PbPb15_grl_name = "data15_hi.periodAllYear_DetStatus-v75-repro20-01_DQDefects-00-02-02_PHYS_HeavyIonP_All_Good.xml";
  const std::string m_pPb16_grl_name = "data16_hip8TeV.periodAllYear_DetStatus-v105-pro22-13_Unknown_PHYS_HeavyIonP_All_Good.xml";
  const std::string m_pp17_grl_name = "data17_5TeV.periodAllYear_DetStatus-v98-pro21-16_Unknown_PHYS_StandardGRL_All_Good_25ns_ignore_GLOBAL_LOWMU.xml";
  const std::string m_PbPb18_grl_name = "data18_hi.periodAllYear_DetStatus-v106-pro22-14_Unknown_PHYS_HeavyIonP_All_Good.xml";
  const std::string m_PbPb18_grl_ignoreToroid_name = "data18_hi.periodAllYear_DetStatus-v106-pro22-14_Unknown_PHYS_HeavyIonP_All_Good_ignore_TOROIDSTATUS.xml";

  const int m_nside = 1;
  const std::string m_oop_fname = "all_fit.root";
  const std::string filePath = "$UserAnalysis_DIR/data/PhotonJetAnalysis";

  std::vector<CP::SystematicSet> photonSysList; // list of systematics associated with photons

  TH1D* m_oop_hMean = nullptr;
  TH1D* m_oop_hSigma = nullptr;

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:

  // this is a standard constructor
  TreeMaker ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  bool isDesynEvent (const int _runNumber, const int _lumiBlock);
  bool is_Outpileup (const xAOD::HIEventShapeContainer& evShCont, const int nTrack);
  uint8_t getSum (const xAOD::TrackParticle& trk, xAOD::SummaryType sumType); 
  bool passSinfigCut (float eta, int cellsigsamp, float cellsig);
  float CellSigCut (float x);
  void ClusterGapCut (std::vector <float>& clus);
  void GapCut (std::vector<float>& trks, std::vector<float>& clus);

  // this is needed to distribute the algorithm to the workers
  ClassDef(TreeMaker, 1);


private:

  /**
   * Returns the equivalent angle in the range 0 to 2pi.
   */
  double InTwoPi (double phi) {
    while (phi < 0 || 2*TMath::Pi () <= phi) {
     if (phi < 0) phi += 2*TMath::Pi ();
     else phi -= 2*TMath::Pi ();
    }
    return phi;
  }
  
  
  /**
   * Returns the difference between two angles in 0 to pi.
   */
  double DeltaPhi (double phi1, double phi2) {
    phi1 = InTwoPi(phi1);
    phi2 = InTwoPi(phi2);
    double dphi = abs(phi1 - phi2);
    while (dphi > TMath::Pi ()) dphi = abs (dphi - 2*TMath::Pi ());
    return dphi;
  }
  
  
  /**
   * Returns dR between two eta, phi coordinates.
   */
  double DeltaR (const double eta1, const double eta2, const double phi1, const double phi2 ) {
   const double deta = eta1 - eta2;
   const double dphi = DeltaPhi (phi1, phi2);
   return sqrt( pow( deta, 2 ) + pow( dphi, 2 ) );
  }


  TTree* m_tree; //!

  // Tree branches 
  int m_b_runNumber; //!
  int m_b_eventNumber; //!
  unsigned int m_b_lumiBlock; //! 
  bool m_b_passesToroid; //!
  bool m_b_isOOTPU; //!
  bool m_b_BlayerDesyn; //!

  float m_b_actualInteractionsPerCrossing; //!
  float m_b_averageInteractionsPerCrossing; //!

  std::vector<float> m_b_mcEventWeights; //!

  // truth event info
  const static int m_max_nTruthEvt = 5;
  int m_b_nTruthEvt; //!
  int m_b_nPart1[m_max_nTruthEvt]; //!
  int m_b_nPart2[m_max_nTruthEvt]; //!
  float m_b_impactParameter[m_max_nTruthEvt]; //!
  int m_b_nColl[m_max_nTruthEvt]; //!
  float m_b_sigmaInelasticNN[m_max_nTruthEvt]; //!
  int m_b_nSpectatorNeutrons[m_max_nTruthEvt]; //!
  int m_b_nSpectatorProtons[m_max_nTruthEvt]; //!
  float m_b_eccentricity[m_max_nTruthEvt]; //!
  float m_b_eventPlaneAngle[m_max_nTruthEvt]; //!

  // photon trigger names
  int m_photon_trig_n; //!
  const std::string* m_photon_trig_name; //!

  const static int m_photon_trig_n_PbPb18 = 2;
  const std::string m_photon_trig_name_PbPb18 [m_photon_trig_n_PbPb18] = {
    "HLT_g30_loose_ion",
    "HLT_g50_loose_ion"
  }; //!

  const static int m_photon_trig_n_pp17 = 2;
  const std::string m_photon_trig_name_pp17 [m_photon_trig_n_pp17] = {
    //"HLT_g15_loose_L1EM7",
    //"HLT_g20_loose_L1EM15",
    //"HLT_g25_loose_L1EM15",
    "HLT_g30_loose_L1EM15",
    "HLT_g35_loose_L1EM15"
  }; //!

  const static int m_photon_trig_n_pPb16 = 2;
  const std::string m_photon_trig_name_pPb16 [m_photon_trig_n_pPb16] = {
    //"HLT_g10_loose",
    //"HLT_g15_loose",
    //"HLT_g20_loose",
    //"HLT_g25_loose",
    "HLT_g30_loose",
    "HLT_g35_loose"
  }; //!

  const static int m_photon_trig_n_PbPb15 = 1;
  const std::string m_photon_trig_name_PbPb15 [m_photon_trig_n_PbPb15] = {
    "HLT_g20_loose_ion"
  }; //!

  const static int m_max_photon_trig_n = std::max (m_photon_trig_n_PbPb18, std::max (m_photon_trig_n_pp17, std::max (m_photon_trig_n_pPb16, m_photon_trig_n_PbPb15)));

  //// jet trigger names
  //int m_jet_trig_n; //!
  //const std::string* m_jet_trig_name; //!
  //static const int m_jet_trig_n_pp17 = 4; //!
  //const std::string m_jet_trig_name_pp17 [m_photon_trig_n_pp17] = {
  //  "HLT_j45",
  //  "HLT_j60",
  //  "HLT_j85",
  //  "HLT_j110"
  //}; //!

  // photon triggers
  bool* m_b_photon_trig_decision; //!
  float* m_b_photon_trig_prescale; //!

  //// jet triggers
  //bool* m_b_jet_trig_decision; //!
  //float* m_b_jet_trig_prescale; //!

  // vertex info
  const static int m_max_nvert = 30;
  int m_b_nvert; //!
  float m_b_vert_x[m_max_nvert]; //!
  float m_b_vert_y[m_max_nvert]; //!
  float m_b_vert_z[m_max_nvert]; //!
  int m_b_vert_ntrk[m_max_nvert]; //!
  int m_b_vert_type[m_max_nvert]; //!
  float m_b_vert_sumpt[m_max_nvert]; //!

  // fcal energy
  float m_b_fcalA_et; //!
  float m_b_fcalC_et; //!
  float m_b_fcalA_et_Cos2; //!
  float m_b_fcalC_et_Cos2; //!
  float m_b_fcalA_et_Sin2; //!
  float m_b_fcalC_et_Sin2; //!
  float m_b_fcalA_et_Cos3; //!
  float m_b_fcalC_et_Cos3; //!
  float m_b_fcalA_et_Sin3; //!
  float m_b_fcalC_et_Sin3; //!
  float m_b_fcalA_et_Cos4; //!
  float m_b_fcalC_et_Cos4; //!
  float m_b_fcalA_et_Sin4; //!
  float m_b_fcalC_et_Sin4; //!

  // ZDC
  float  m_b_ZdcCalibEnergy_A; //!
  float  m_b_ZdcCalibEnergy_C; //!
  bool   m_b_L1_ZDC_A; //!
  bool   m_b_L1_ZDC_A_tbp; //!
  bool   m_b_L1_ZDC_A_tap; //!
  bool   m_b_L1_ZDC_A_tav; //!
  float  m_b_L1_ZDC_A_prescale; //!
  bool   m_b_L1_ZDC_C; //!
  bool   m_b_L1_ZDC_C_tbp; //!
  bool   m_b_L1_ZDC_C_tap; //!
  bool   m_b_L1_ZDC_C_tav; //!
  float  m_b_L1_ZDC_C_prescale; //!

  // sum of gaps for UPC background
  float m_b_clusterOnly_sum_gap_A; //!
  float m_b_clusterOnly_sum_gap_C; //!
  float m_b_clusterOnly_edge_gap_A; //!
  float m_b_clusterOnly_edge_gap_C; //!
  float m_b_sum_gap_A; //!
  float m_b_sum_gap_C; //!
  float m_b_edge_gap_A; //!
  float m_b_edge_gap_C; //!

  // photon info
  const static int m_max_photon_n = 1000;
  int m_b_photon_n; //!
  float m_b_photon_pt_precalib[m_max_photon_n]; //!
  float m_b_photon_pt[m_max_photon_n]; //!
  float m_b_photon_eta[m_max_photon_n]; //!
  float m_b_photon_etaBE[m_max_photon_n]; //!
  float m_b_photon_phi[m_max_photon_n]; //!
  bool m_b_photon_matched[m_max_photon_trig_n][m_max_photon_n]; //!
  bool m_b_photon_tight[m_max_photon_n]; //!
  bool m_b_photon_medium[m_max_photon_n]; //!
  bool m_b_photon_loose[m_max_photon_n]; //!
  unsigned int m_b_photon_isem[m_max_photon_n]; //!
  int m_b_photon_convFlag[m_max_photon_n]; //!
  float m_b_photon_Rconv[m_max_photon_n]; //!
  float m_b_photon_etcone20[m_max_photon_n]; //!
  float m_b_photon_etcone30[m_max_photon_n]; //!
  float m_b_photon_etcone40[m_max_photon_n]; //!
  float m_b_photon_topoetcone20[m_max_photon_n]; //!
  float m_b_photon_topoetcone30[m_max_photon_n]; //!
  float m_b_photon_topoetcone40[m_max_photon_n]; //!
  float m_b_photon_Rhad1[m_max_photon_n]; //!
  float m_b_photon_Rhad[m_max_photon_n]; //!
  float m_b_photon_e277[m_max_photon_n]; //!
  float m_b_photon_Reta[m_max_photon_n]; //!
  float m_b_photon_Rphi[m_max_photon_n]; //!
  float m_b_photon_weta1[m_max_photon_n]; //!
  float m_b_photon_weta2[m_max_photon_n]; //!
  float m_b_photon_wtots1[m_max_photon_n]; //!
  float m_b_photon_f1[m_max_photon_n]; //!
  float m_b_photon_f3[m_max_photon_n]; //!
  float m_b_photon_fracs1[m_max_photon_n]; //!
  float m_b_photon_DeltaE[m_max_photon_n]; //!
  float m_b_photon_Eratio[m_max_photon_n]; //!
  float m_b_photon_pt_sys[m_max_photon_n]; //!
  float m_b_photon_eta_sys[m_max_photon_n]; //!
  float m_b_photon_phi_sys[m_max_photon_n]; //!

  // truth photon info
  const static int m_max_truth_photon_n = 1000;
  int m_b_truth_photon_n; //!
  float m_b_truth_photon_pt[m_max_truth_photon_n]; //!
  float m_b_truth_photon_eta[m_max_truth_photon_n]; //!
  float m_b_truth_photon_phi[m_max_truth_photon_n]; //!
  int m_b_truth_photon_barcode[m_max_truth_photon_n]; //!

  // egamma calorimeter cluster info, see https://ucatlas.github.io/RootCoreDocumentation/2.4.28/dc/d4b/CaloCluster__v1_8h_source.html
  const static int m_max_cluster_n = 100;
  int m_b_cluster_n; //!
  float m_b_cluster_pt[m_max_cluster_n]; //!
  float m_b_cluster_et[m_max_cluster_n]; //!
  float m_b_cluster_eta[m_max_cluster_n]; //!
  float m_b_cluster_phi[m_max_cluster_n]; //!
  float m_b_cluster_energyBE[m_max_cluster_n]; //!
  float m_b_cluster_etaBE[m_max_cluster_n]; //!
  float m_b_cluster_phiBE[m_max_cluster_n]; //!
  float m_b_cluster_calE[m_max_cluster_n]; //!
  float m_b_cluster_calEta[m_max_cluster_n]; //!
  float m_b_cluster_calPhi[m_max_cluster_n]; //!
  int m_b_cluster_size[m_max_cluster_n]; //!
  int m_b_cluster_status[m_max_cluster_n]; //!

  // tracking info (0th or primary vertex only)
  const static int m_max_ntrk = 10000;
  int m_b_ntrk; //!
  float m_b_trk_pt[m_max_ntrk]; //!
  float m_b_trk_eta[m_max_ntrk]; //!
  float m_b_trk_phi[m_max_ntrk]; //!
  float m_b_trk_charge[m_max_ntrk]; //!
  bool m_b_trk_HItight[m_max_ntrk]; //!
  bool m_b_trk_HIloose[m_max_ntrk]; //!
  bool m_b_trk_MinBias[m_max_ntrk]; //!
  bool m_b_trk_TightPrimary[m_max_ntrk]; //!
  float m_b_trk_d0[m_max_ntrk]; //!
  float m_b_trk_d0sig[m_max_ntrk]; //!
  float m_b_trk_z0[m_max_ntrk]; //!
  float m_b_trk_z0sig[m_max_ntrk]; //!
  float m_b_trk_theta[m_max_ntrk]; //!
  float m_b_trk_vz[m_max_ntrk]; //!
  int m_b_trk_nBLayerHits[m_max_ntrk]; //!
  int m_b_trk_nBLayerSharedHits[m_max_ntrk]; //!
  int m_b_trk_nPixelHits[m_max_ntrk]; //!
  int m_b_trk_nPixelDeadSensors[m_max_ntrk]; //!
  int m_b_trk_nPixelSharedHits[m_max_ntrk]; //!
  int m_b_trk_nSCTHits[m_max_ntrk]; //!
  int m_b_trk_nSCTDeadSensors[m_max_ntrk]; //!
  int m_b_trk_nSCTSharedHits[m_max_ntrk]; //!
  int m_b_trk_nTRTHits[m_max_ntrk]; //!
  int m_b_trk_nTRTSharedHits[m_max_ntrk]; //!
  float m_b_trk_prob_truth[m_max_ntrk]; //!
  float m_b_trk_truth_pt[m_max_ntrk]; //!
  float m_b_trk_truth_eta[m_max_ntrk]; //!
  float m_b_trk_truth_phi[m_max_ntrk]; //!
  float m_b_trk_truth_charge[m_max_ntrk]; //!
  int m_b_trk_truth_type[m_max_ntrk]; //!
  int m_b_trk_truth_orig[m_max_ntrk]; //!
  int m_b_trk_truth_barcode[m_max_ntrk]; //!
  int m_b_trk_truth_pdgid[m_max_ntrk]; //!
  float m_b_trk_truth_vz[m_max_ntrk]; //!
  int m_b_trk_truth_nIn[m_max_ntrk]; //!
  bool m_b_trk_truth_isHadron[m_max_ntrk]; //!

  // truth track info
  const static int m_max_truth_trk_n = 10000;
  int m_b_truth_trk_n; //!
  float m_b_truth_trk_pt[m_max_truth_trk_n]; //!
  float m_b_truth_trk_eta[m_max_truth_trk_n]; //!
  float m_b_truth_trk_phi[m_max_truth_trk_n]; //!
  float m_b_truth_trk_charge[m_max_truth_trk_n]; //!
  int m_b_truth_trk_pdgid[m_max_truth_trk_n]; //!
  int m_b_truth_trk_barcode[m_max_truth_trk_n]; //!
  bool m_b_truth_trk_isHadron[m_max_truth_trk_n]; //!

  // jet info
  const static int m_max_akt4hi_jet_n = 40;
  int m_b_akt4hi_jet_n; //!
  float m_b_akt4hi_jet_pt[m_max_akt4hi_jet_n]; //!
  float m_b_akt4hi_jet_eta[m_max_akt4hi_jet_n]; //!
  float m_b_akt4hi_jet_phi[m_max_akt4hi_jet_n]; //!
  float m_b_akt4hi_jet_e[m_max_akt4hi_jet_n]; //!
  bool m_b_akt4hi_jet_LooseBad[m_max_akt4hi_jet_n]; //!

  // truth jet info
  const static int m_max_akt4_truth_jet_n = 40;
  int m_b_akt4_truth_jet_n; //!
  float m_b_akt4_truth_jet_pt[m_max_akt4_truth_jet_n]; //!
  float m_b_akt4_truth_jet_eta[m_max_akt4_truth_jet_n]; //!
  float m_b_akt4_truth_jet_phi[m_max_akt4_truth_jet_n]; //!
  float m_b_akt4_truth_jet_e[m_max_akt4_truth_jet_n]; //!



  // MC Truth Classifier
  MCTruthClassifier* m_mcTruthClassifier; //!

  // GRL tools
  GoodRunsListSelectionTool* m_PbPb15_grl; //!
  GoodRunsListSelectionTool* m_pPb16_grl; //!
  GoodRunsListSelectionTool* m_pp17_grl; //!
  GoodRunsListSelectionTool* m_PbPb18_grl; //!
  GoodRunsListSelectionTool* m_PbPb18_ignoreToroid_grl; //!

  // Triggering tools
  Trig::TrigDecisionTool* m_trigDecisionTool; //!
  TrigConf::xAODConfigTool* m_trigConfigTool; //!
  Trig::MatchingTool* m_trigMatchingTool; //!

  // ZDC analysis tool
  ZDC::ZdcAnalysisTool* m_zdcAnalysisTool; //!

  // Egamma tools
  AsgPhotonIsEMSelector* m_photonTightIsEMSelector; //!
  AsgPhotonIsEMSelector* m_photonMediumIsEMSelector; //!
  AsgPhotonIsEMSelector* m_photonLooseIsEMSelector; //!
  CP::EgammaCalibrationAndSmearingTool* m_egammaPtEtaPhiECorrector; //!

  // Track selection tools
  InDet::InDetTrackSelectionTool* m_trackSelectionToolTightPrimary; //!
  InDet::InDetTrackSelectionTool* m_trackSelectionToolMinBias; //!
  InDet::InDetTrackSelectionTool* m_trackSelectionToolHITight; //!
  InDet::InDetTrackSelectionTool* m_trackSelectionToolHILoose; //!

  // Jet cleaning tool
  JetCleaningTool* m_jetCleaningTool; //!

  // EtaJES calibration tool - EM scale
  JetCalibrationTool* m_Akt4HI_EM_EtaJES_CalibTool; //!
  // 2015 insitu + cross calibration tool
  JetCalibrationTool* m_Akt4HI_Insitu_CalibTool; //!

};


#endif
