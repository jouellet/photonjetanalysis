#include <EventLoop/DirectDriver.h>
#include <EventLoop/Job.h>
#include <PhotonJetAnalysis/TreeMaker.h>
#include <TSystem.h>
#include <SampleHandler/ScanDir.h>
#include <SampleHandler/ToolsDiscovery.h>
#include <EventLoop/LSFDriver.h>


void ATestRun ()
{
  // Set up the job for xAOD access:
  xAOD::Init().ignore();

  // create a new sample handler to describe the data files we use
  SH::SampleHandler sh;

  const char* inputFilePath = gSystem->ExpandPathName ("/afs/cern.ch/work/j/jeouelle/data17_5TeV/");
  SH::ScanDir ().filePattern ("*AOD*.1").scan (sh, inputFilePath); // data17_5TeV run 340718
  //const char* inputFilePath = gSystem->ExpandPathName ("/afs/cern.ch/work/j/jeouelle/data18_hi/");
  //SH::ScanDir ().filePattern ("*AOD*.1").scan (sh, inputFilePath);

  // set the name of the tree in our files
  // in the xAOD the TTree containing the EDM containers is "CollectionTree"
  sh.setMetaString ("nc_tree", "CollectionTree");

  // further sample handler configuration may go here

  // print out the samples we found
  sh.print ();

  // this is the basic description of our job
  EL::Job job;
  job.sampleHandler (sh); // use SampleHandler in this job
  //job.options()->setDouble (EL::Job::optMaxEvents, 500); // for testing purposes, limit to run over the first 500 events only! // for tutorial
  
  std::string output = "Tree";
  EL::OutputStream outputFile  (output);
  job.outputAdd (outputFile);

  // add our algorithm to the job
  TreeMaker *alg = new TreeMaker;
  alg->m_collisionSystem = pp17;
  alg->m_dataType = Collisions;
  alg->m_outputName = output;
  

  // set the name of the algorithm (this is the name use with messages)
  alg->SetName ("PhotonJetAnalysis");

  job.algsAdd (alg);

  // Standard driver
  EL::DirectDriver driver;

  //// Condor driver
  //EL::CondorDriver driver;
  //job.options()->setDouble (EL::Job::optFilesPerWorker, 5);
  //driver.options()->setString (EL::Job::optSubmitFlags, "-terse");
  //driver.shellInit = "export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase && source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh";

  /***** Job submission *****/
 
  job.options()->setDouble(EL::Job::optRemoveSubmitDir, 1); 
  // process the job using the driver
  driver.submit (job, "submit/submitDir"); // for direct driver
  //driver.submitOnly (job, "output/submitDir");   // for running on condor

  /***** End job submission *****/

}
